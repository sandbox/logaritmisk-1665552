<?php


/**
 *
 */
abstract class Governor {

  /**
   *
   */
  public function options() {
    return array(
      '_enabled' => FALSE,
    );
  }

  /**
   *
   */
  public function settings(&$form, $config) {

  }

  /**
   *
   */
  public function process() {
    return FALSE;
  }

}
