<?php
/**
 * @file
 */


$plugin = array(
  'name' => t('Weekdays'),
  'description' => t('Choose weekdays.'),
  'package' => 'Date',
  'handler' => array(
    'class' => 'DateWeekday',
  ),
);

/**
 *
 */
class DateWeekday extends Governor {

  /**
   *
   */
  public function options() {
    return parent::options() + array(
      'weekdays' => array(),
    );
  }

  /**
   *
   */
  public function settings(&$form, $config) {
    $form['weekdays'] = array(
      '#type' => 'checkboxes',
      '#options' => array(
        'monday' => t('Monday'),
        'tuesday' => t('Tuesday'),
        'wednesday' => t('Wednesday'),
        'thursday' => t('Thursday'),
        'friday' => t('Friday'),
        'saturday' => t('Saturday'),
        'sunday' => t('Sunday'),
      ),
      '#default_value' => (array) $config['weekdays'],
    );
  }

  /**
   *
   */
  public function process($config, $contexts) {
    if (!isset($contexts['date'])) {
      $date = getdate();
    }
    else {
      $date = getdate($contexts['date']);
    }

    return $config['weekdays'][strtolower($date['weekday'])] !== 0;
  }

}
